//Menu
// Sticky Header
$(window).scroll(function() {

    if ($(window).scrollTop() > 0) {
        $('.main_h').addClass('sticky');
    } else {
        $('.main_h').addClass('sticky');
    }
});

// Mobile Navigation
$('.mobile-toggle').click(function() {
    if ($('.main_h').hasClass('open-nav')) {
        $('.main_h').removeClass('open-nav');
    } else {
        $('.main_h').addClass('open-nav');
    }
});

$('.main_h li a').click(function() {
    if ($('.main_h').hasClass('open-nav')) {
        $('.navigation').removeClass('open-nav');
        $('.main_h').removeClass('open-nav');
    }
});

// navigation scroll lijepo radi materem
$('nav a').click(function(event) {
    var id = $(this).attr("href");
    var offset = 70;
    var target = $(id).offset().top - offset;
    $('html, body').animate({
        scrollTop: target
    }, 500);
    event.preventDefault();
});

//Validación del DUI
var pattern = /\d/,
    caja = document.getElementById("DUI");
 
caja.addEventListener("keypress", function(e){
    if (!pattern.test(String.fromCharCode(e.keyCode)) || this.value.length == 10)
        e.preventDefault();
    if (this.value.length === 8)
        this.value += "-";
}, false);

//Validación de la Tarjeta de Credito
var pattern = /\d/,
    caja = document.getElementById("TC");
 
caja.addEventListener("keypress", function(e){
    if (!pattern.test(String.fromCharCode(e.keyCode)) || this.value.length == 19)
        e.preventDefault();
    if (this.value.length === 4)
        this.value += " ";
    
     if (this.value.length === 9)
        this.value += " ";
    
     if (this.value.length === 14)
        this.value += " ";
}, false);

//Validación (opcional) de la verificacion de la tarjeta de credito
var pattern = /\d/,
    caja = document.getElementById("Veri");
 
caja.addEventListener("keypress", function(e){
    if (!pattern.test(String.fromCharCode(e.keyCode)) || this.value.length == 3)
        e.preventDefault();
    if (this.value.length === 3)
        this.value += "";

}, false);

//Validación del Teléfono
var pattern = /\d/,
    caja = document.getElementById("Telefono");
 
caja.addEventListener("keypress", function(e){
    if (!pattern.test(String.fromCharCode(e.keyCode)) || this.value.length == 9)
        e.preventDefault();
    if (this.value.length === 4)
        this.value += "-";
}, false);
