document.querySelector('#btnEntrar').addEventListener('click', iniciarSesion);


function iniciarSesion(){
    var userN = '';
    var userContra = '';
    var bAccess = false;
    
    userN = document.querySelector('#N_usuario').value;
    userContra = document.querySelector('#contra').value;
    
    bAccess = Validar(userN,userContra);
    
    
    if(bAccess == true){
        ingresar();
    }
}


function ingresar(){
    
    var rol = localStorage.getItem('rolUserActivo');
    switch(rol){
        case '1':
            window.location.href = '../html/admin.html';
        break;
        case '2':
            window.location.href = '../html/cliente/index.html';
        break;
        default:
        window.location.href = '../html/index.html';
        break;
    }
}