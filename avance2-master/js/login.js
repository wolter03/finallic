function ObtenlistaUsuarios(){
    var listaUsers = JSON.parse(localStorage.getItem('ListaUsuarios'))
    
    if(listaUsers == null){
        listaUsers = [
            //id, Nombres, Apellidos, Fecha Nacimiento, Correo, Nombre de Usuario, Contraseña, Rol
            ['1','Walter Alexis','Carabantes Gutiérrez','16-03-2001','waltergutierrez765@gmail.com','wolter','123','1'],['2','Alejandro Antonio','Alas Linares','23-05-2001','aletoshi@gmail.com','Alejim','456','2']
        ]
    }
    return listaUsers;
}

    
function Validar(N_usuario, contra){
    
    var listaUsers = ObtenlistaUsuarios();
    var bAccess = false;
    
    for(var i = 0; i < listaUsers.length; i++){
        if(N_usuario == listaUsers[i][5] && contra == listaUsers[i][6]){
            bAccess = true;
            localStorage.setItem('userActivo', listaUsers[i][1] + ' ' + listaUsers[i][2]);
            localStorage.setItem('rolUserActivo', listaUsers[i][7]);
        }
    }
    return bAccess;   
}
