var miembro = new Object();

function iniciar() {
    var enviar = document.getElementById("btnRegistro");
    if (enviar.addEventListener) {
        enviar.addEventListener("click", function () {
            createObject(document.formulario_registro);

        }, false);
    } else {
        enviar.attachEvent("onclick", function () {
            createObject(document.formulario_registro);
            
        });
    }
}


function createObject(form) {
    var fecha_nac = form.fecha_nac.value;
    var fecha = new Date();
    var hoy = moment(fecha).format('YYYY-MM-DD');
    miembro.nombres = form.nombres.value;
    miembro.apellidos = form.apellidos.value;
    miembro.DUI = form.DUI.value;
    miembro.correo = form.correo.value;
    miembro.username = form.username.value;
    miembro.fon = form.fon.value;
    miembro.contra = form.contra.value;
    miembro.id = generarID(miembro.p_apellido, miembro.s_apellido, miembro.fecha);
    if (miembro.nombres == "" || miembro.apellidos == "" || miembro.DUI == "" || miembro.correo == "" || miembro.username == "" || miembro.fon == "" || miembro.contra == "") {
        alert("Por favor, complete todos los campos")
    } else {
        var valido;
        var re = /^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
        if (re.test(miembro.correo)) {
            valido = true;
        }

        if (miembro.fecha < hoy) {
            if(valido == true){
                alert("Usuario agregado");
               mostrarPro(miembro, "InfoMiembro");
               
            }else{
                alert("El correo ingresado no es válido");
            }
        } else {
            alert("La fecha de nacimiento no puede ser mayor a la fecha actual");
        }
    }
    

}

function generarID(apellido1, apellido2, fecha) {
    var apellido1 = apellido1;
    var apellido2 = apellido2;
    var fecha = fecha;
    var ID;

    if (apellido2 == "") {
        return ID = apellido1.substr(0, 1).toUpperCase() +
            apellido1.substr(0, 1).toUpperCase() +
            fecha.substr(0, 4) +
            Math.floor(Math.random() * ((10001) - 1000) + 1000);
    } else {
        return ID = apellido1.substr(0, 1).toUpperCase() +
            apellido2.substr(0, 1).toUpperCase() +
            fecha.substr(0, 4) +
            Math.floor(Math.random() * ((10001) - 1000) + 1000);
    }
}

function mostrarPro(objeto, objName) {
    var infMiembro = "";
    var tblMiembro = "";
    for (var i in objeto) {
        infMiembro = infMiembro + objName + "." + i + " = " + objeto[i] + "\n";
    }
        var abc = document.getElementById("resu");

        abc.innerHTML += "<tr>";
        abc.innerHTML += "<td>" + miembro.id + "</td>" + "<td>" + miembro.nombres + "</td>" + "<td>" + miembro.p_apellido + "</td>" + "<td>" + miembro.s_apellido + "</td>" + "<td>" + miembro.correo + "</td>" + "<td>" + miembro.contraseña + "</td>" +  "<td>" + miembro.fecha + "</td>";
        abc.innerHTML += "</tr>";
}

if (window.addEventListener) {
    window.addEventListener("load", iniciar, false);
} else if (window.attachEvent) {
    window.attachEvent("onload", iniciar);
}
