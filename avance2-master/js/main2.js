
(function () {

    var formulario = document.formulario_registro,
        elementos = formulario.elements;

    var validarInputs = function () {
        for (var i = 0; i < elementos.length; i++) {
            if (elementos[i].type == "text" || elementos[i].type == "email" || elementos[i].type == "password") {
                if (elementos[i].value.length == 0) {
                    alert('El campo ' + elementos[i].name + ' esta incompleto');
                    elementos[i].className = elementos[i].className + " error";
                    return false;
                } else {
                    elementos[i].className = elementos[i].className.replace(" error", "");
                }
            }
        }

        if (elementos.pass.value !== elementos.pass2.value) {
            elementos.pass.value = "";
            elementos.pass2.value = "";
            elementos.pass.className = elementos.pass.className + " error";
            elementos.pass2.className = elementos.pass2.className + " error";
            alert("Las contraseñas no coinciden");
        } else {
            elementos.pass.className = elementos.pass.className.replace(" error", "");
            elementos.pass2.className = elementos.pass2.className.replace(" error", "");
        }

        return true;
    };

    var enviar = function (e) {
        if (!validarInputs()) {
            console.log('Falto validar los Input');
            e.preventDefault();
        } else if (!validarRadios()) {
            console.log('Falto validar los Radio Button');
            e.preventDefault();
        } else if (!validarCheckbox()) {
            console.log('Falto validar Checkbox');
            e.preventDefault();
        } else {
            console.log('Envia');
            e.preventDefault();
        }
    };

    formulario.addEventListener("submit", enviar);

    for (var i = 0; i < elementos.length; i++) {
        if (elementos[i].type == "text" || elementos[i].type == "email" || elementos[i].type == "password") {
            elementos[i].addEventListener("focus", focusInput);
            elementos[i].addEventListener("blur", blurInput);
        }
    }

}())
