llenarT();

function llenarT(){
    
    
    var tbody = document.querySelector('#tablaProductos tbody');
    
    tbody.innerHTML = '';
    
    var id = JSON.parse(localStorage.getItem('id_pro')),
        nombre = JSON.parse(localStorage.getItem('nombre_pro')),
        descripcion = JSON.parse(localStorage.getItem('descrip_pro')),
        foto = JSON.parse(localStorage.getItem('foto_pro')),
        precio = JSON.parse(localStorage.getItem('precio_pro'));
    
    
    var nCantidadPro = id.length;
    
    for(var i = 0; i < nCantidadPro; i++){
        
        var fila = document.createElement('tr');
        
        var celdaId = document.createElement('td'),
        celdaNombre = document.createElement('td'),
        celdaDesc = document.createElement('td'),
        celdaFoto = document.createElement('td'),
        celdaPrecio = document.createElement('td'),
        celdaModi = document.createElement('td'),
        enlace = document.createElement('a');
        
        celdaModi.setAttribute('class', 'btnModificar');
        celdaModi.setAttribute('id',i);
        enlace.href = 'modificar.html?id' + '=' + i;
        
        
        var ndoTxtId= document.createTextNode(id[i]),
            ndoTxtNombre= document.createTextNode(nombre[i]),
            ndoTxtDesc= document.createTextNode(descripcion[i]),
            ndoTxtFoto= document.createTextNode(foto[i]),
            ndoTxtPrecio= document.createTextNode(precio[i]),
            ndoTxtModi = document.createTextNode('Modificar');
            
            
        celdaId.appendChild(ndoTxtId);
        celdaNombre.appendChild(ndoTxtNombre);
        celdaDesc.appendChild(ndoTxtDesc);
        celdaFoto.appendChild(ndoTxtFoto);
        celdaPrecio.appendChild(ndoTxtPrecio);
        enlace.appendChild(ndoTxtModi);
        celdaModi.appendChild(enlace);
        
        
        fila.appendChild(celdaId);
        fila.appendChild(celdaNombre);
        fila.appendChild(celdaDesc);
        fila.appendChild(celdaFoto);
        fila.appendChild(celdaPrecio);
        fila.appendChild(celdaModi);
        
        tbody.appendChild(fila);
            
    }
}