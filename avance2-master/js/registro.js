
var nombree = [],
    apellido = [],
    ddui = [],
    corre = [],
    username = [],
    ffon = [],
    wordpass = [];

var btnRegistrar = document.querySelector('#btnRegistro');

btnRegistro.addEventListener('click', registrarPro);


function registrarPro(){
    
    var name = document.querySelector('#nombres').value,
        ape = document.querySelector('#apellidos').value,
        dui = document.querySelector('#DUI').value,
        email = document.querySelector('#correo').value,
        user = document.querySelector('#username').value,
        celular = document.querySelector('#fon').value,
        pass = document.querySelector('#contra').value;
    
    
    nombree.push(name);
    apellido.push(ape);
    ddui.push(dui);
    corre.push(email);
    username.push(user);
    ffon.push(celular);
    wordpass.push(pass);
    
    

    
}

function guardarInfo(paNombre, paApe, paDUI, paCorre, paUser, paCel, paContra, paRol){
    localStorage.setItem('nombre_cli', JSON.stringify(paNombre));
    localStorage.setItem('apellido_cli', JSON.stringify(paApe));
    localStorage.setItem('dui_cli', JSON.stringify(paDUI));
    localStorage.setItem('correo_cli', JSON.stringify(paCorre));
    localStorage.setItem('username_cli', JSON.stringify(paUser));
    localStorage.setItem('celular_cli', JSON.stringify(paCel));
    localStorage.setItem('contra_cli', JSON.stringify(paContra));
    localStorage.setItem('2', JSON.stringify('2'));

}

function valida() {
    
    var name = document.querySelector('#nombres').value,
        ape = document.querySelector('#apellidos').value,
        dui = document.querySelector('#DUI').value,
        email = document.querySelector('#correo').value,
        user = document.querySelector('#username').value,
        celular = document.querySelector('#fon').value,
        pass = document.querySelector('#contra').value;
    var valido = false;
    
    if(name == "" || ape == "" || dui == "" || email == "" || user == "" || celular == "" || pass == ""){
        alert("Complete todos los campos");
    }   
    
    
    else{
    guardarInfo(nombree, apellido, ddui, corre, username, ffon, wordpass, '2');
    window.location.href = '../html/cliente.html';
    }
}

//Validación del DUI
var pattern = /\d/,
    caja = document.getElementById("DUI");
 
caja.addEventListener("keypress", function(e){
    if (!pattern.test(String.fromCharCode(e.keyCode)) || this.value.length == 10)
        e.preventDefault();
    if (this.value.length == 8)
        this.value += "-";
}, false);


//Validación del Teléfono
var pattern = /\d/,
    caja = document.getElementById("fon");
caja.addEventListener("keypress", function(e){
    if (!pattern.test(String.fromCharCode(e.keyCode)) || this.value.length == 9)
        e.preventDefault();
    if (this.value.length === 4)
        this.value += "-";
}, false);
    